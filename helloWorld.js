function worship(god){
    console.log(god);
}
worship(' Jai Ganeshji    Jai Hanumanji     Jai Krishna bhagwanji   Jai Saraswati mataji   Jai Laxmi mataji    Jai Parvati mataji    Jai Mahadevji   Jai Gurudev')
console.log('hello World');

var message = ' Jai Ganeshji    Jai Hanumanji     Jai Krishna bhagwanji   Jai Saraswati mataji   Jai Laxmi mataji    Jai Parvati mataji    Jai Mahadevji   Jai Gurudev ';

// console.log(window)  //ReferenceError: window is not defined
// console.log(document) //ReferenceError: document is not defined

// =====================================================================================================
console.log(console) //
//  Console {
//     log: [Function: bound consoleCall],debug: [Function: bound consoleCall],
//     info: [Function: bound consoleCall],warn: [Function: bound consoleCall],
//     error: [Function: bound consoleCall],dir: [Function: bound consoleCall],
//     time: [Function: bound consoleCall],timeEnd: [Function: bound consoleCall],
//     trace: [Function: bound consoleCall],assert: [Function: bound consoleCall],
//     clear: [Function: bound consoleCall],count: [Function: bound consoleCall],
//     countReset: [Function: bound countReset],group: [Function: bound consoleCall],
//     groupCollapsed: [Function: bound consoleCall],groupEnd: [Function: bound consoleCall],
//     Console: [Function: Console],dirxml: [Function: dirxml],table: [Function: table],
//     markTimeline: [Function: markTimeline],profile: [Function: profile],profileEnd: [Function: profileEnd],
//     timeline: [Function: timeline],timelineEnd: [Function: timelineEnd],timeStamp: [Function: timeStamp],
//     context: [Function: context],[Symbol(counts)]: Map {} }
// console is global and many more global object like setTimeout()

// console.clear(); // Woow clears terminal executed content (output) 


// =====================================================================================================

// console.log(setTimeout());    //timers.js:434
// timers.js:419
//     throw new errors.TypeError('ERR_INVALID_CALLBACK');
//     ^

// TypeError [ERR_INVALID_CALLBACK]: Callback must be a function

setTimeout(function(){return (5 + 5); },100);

// setTimeout is used to call a function after some second delay..
// clearTimeout (stop setTimeout)
// setInterval (repeatedly call a function after some delay)
// clearInterval (stop setInterval)


global.console.log("Woow Node Js Language");
console.log(global.message); // undefined as not added to global object

// =====================================================================================================

console.log(module);
// Module {
    // id: '.',
    // exports: {},
    // parent: null,
    // filename: '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/helloWorld.js',
    // loaded: false,
    // children: [],
    // paths: 
    //  [ '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/node_modules',
    //    '/home/jaideeplohar/Documents/reactWorkspace/node_modules',
    //    '/home/jaideeplohar/Documents/node_modules',
    //    '/home/jaideeplohar/node_modules',
    //    '/home/node_modules',
    //    '/node_modules' ] }
//   

global.console.log("=====================================================================================================");


// for acessing other module we use require method by passing path of global module to be accessed..
var logger = require('./logger')  // returns object that is exported from these target module..

console.log(logger);
logger.log("hello world");
// logger.endPoint = "temp";
// logger.log("hello world"); 

const loggers = require('./logger')  // returns object that is exported from these target module..
// because we don't want to accidentally overwrite a module
// loggers = 1;
//jshint app.js
console.log(typeof (loggers))

  

