var url = 'http://mylogger.io/log';

function log(message){
    //Send a HTTP request
    console.log(message);
    console.log(url);
}

module.exports.log = log; // public interface

// module.exports.endPoint = url; // private interface, implementation detail
                                // other modules don't want to know any detail about URL
                                // they just want to call log()

// ============================================================================
// module.exports = log // exporting just function instead of object
    // [Function: log]
    // /home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/helloWorld.js:78
    // logger.log("hello world");
        //    ^
    // 
    // TypeError: logger.log is not a function
    // 
// anything that is exported is added to global object i.e; available at global scope.. 


// Let us understand it with a real-life example a DVD player as its public interface is play/ pause/ stop button
    // But internally it consist of mother board and mush more hardware which may vary from model to model
    // So similarly here url is like mother board which is private and the user does not need detail about it..
    // They just simply call log("message ") function like public interface

// ============================================================================

console.log("In logger.js================",module);
// In logger.js================ Module {
    // id: '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/logger.js',
    // exports: [Function: log],
    // parent: 
    //  Module {
    //    id: '.',
    //    exports: {},
    //    parent: null,
    //    filename: '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/helloWorld.js',
    //    loaded: false,
    //    children: [ [Circular] ],
    //    paths: 
        // [ '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/node_modules',
        //   '/home/jaideeplohar/Documents/reactWorkspace/node_modules',
        //   '/home/jaideeplohar/Documents/node_modules',
        //   '/home/jaideeplohar/node_modules',
        //   '/home/node_modules',
        //   '/node_modules' ] },
    // filename: '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/logger.js',
    // loaded: false,
    // children: [],
    // paths: 
    //  [ '/home/jaideeplohar/Documents/reactWorkspace/demonodeworkspace/node_modules',
    //    '/home/jaideeplohar/Documents/reactWorkspace/node_modules',
    //    '/home/jaideeplohar/Documents/node_modules',
    //    '/home/jaideeplohar/node_modules',
    //    '/home/node_modules',
    //    '/node_modules' ] }
//   
